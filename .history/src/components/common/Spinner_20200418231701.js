import React from 'react';
import './Spinner.css';

const Spinner = () => {
  return <div className="loading">Loading...</div>;
};

export default Spinner;
